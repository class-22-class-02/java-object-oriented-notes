（1）父类Graphic图形

- public double area()方法：返回0.0
- public double perimeter()方法：返回0.0
- public String getInfo()方法，返回图形面积和图形周长

（2）子类Circle圆继承Graphic图形

- 包含属性：radius，属性私有化
- 包含get/set方法
- 重写area()求面积方法
- 重写perimeter()求周长方法
- 重写getInfo()方法，返回圆的半径，面积和周长

（3）子类矩形Rectangle继承Graphic图形

- 包含属性：length、width，属性私有化
- 包含get/set方法
- 重写area()求面积方法
- 重写perimeter()求周长方法
- 重写getInfo()方法，返回长和宽，面积、周长信息

（4）在测试类中，新建一个比较图形面积的方法，再建一个比较图形周长的方法，main方法中创建多个圆和矩形对象,再调用方法比较他们的周长或面积。



```java
//Graphic类
public class Graphic {

    public double area(){
        return 0.0;
    }
    public double perimeter() {
        return 0.0;
    }

    public String getInfo(){
        return "面积:"+area()+"周长"+perimeter();
    }
}

//圆类,继承G
class Circle extends Graphic{
    private double radius;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double area() {
        return Math.PI*(radius*radius);
    }

    @Override
    public double perimeter() {
        return 3.14*(2*radius);
    }

    //@Override
    public String getInfo() {
        return "面积:"+area()+"周长"+perimeter();
    }
}

//矩形类.继承G类
class Rectangle extends Graphic{
    private double length,width;

    public Rectangle() {
    }

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }



    @Override
    public double area() {
        return length*width;
    }

    @Override
    public double perimeter() {
        return (width+length)*2;
    }

    @Override
    public String getInfo() {
        return "周长:"+perimeter()+"面积"+area();
    }
}

//测试类
public class Test {

    public static void main(String[] args) {
        Graphic yuan1 = new Circle(5);
        Graphic yuan2 = new Circle(6);
        Graphic yuan3 = new Circle(7);
        Graphic[] g1 = new Graphic[3];
        g1[0] = yuan1;
        g1[1] = yuan2;
        g1[2] = yuan3;
        System.out.println("第一个圆的面积和周长:" + yuan1.getInfo());
        System.out.println("第二个圆的面积和周长:" + yuan2.getInfo());
        System.out.println("第三个圆的面积和周长:" + yuan3.getInfo());


        System.out.println("----------------------------");
        Graphic chang1 = new Rectangle(10, 3);
        Graphic chang2 = new Rectangle(4, 5);
        Graphic chang3 = new Rectangle(9, 5);

        Graphic[] g2 = new Graphic[3];
        g2[0] = chang1;
        g2[1] = chang2;
        g2[2] = chang3;
        System.out.println("第一个矩形的面积和周长:" + chang1.getInfo());
        System.out.println("第二个矩形的面积和周长:" + chang2.getInfo());
        System.out.println("第三个矩形的面积和周长:" + chang3.getInfo());
        System.out.println(" ");
        getMaxArea(g1,g2);
    }

    static void getMaxArea(Graphic[] g1,Graphic[] g2) {
        //圆的比较
        Graphic maxGraphic1 = g1[0];
        for (int i = 1; i < g1.length; i++) {
            if (g1[i].area() > maxGraphic1.area()) {
                maxGraphic1 = g1[i];
            }
            if (g1[i].perimeter() > maxGraphic1.perimeter()) {
                maxGraphic1 = g1[i];
            }

            //return (yuan1.area() > yuan2.area() ? yuan1.area() :
            //        yuan2.area())> yuan3.area()? (yuan1.area() > yuan2.area() ? yuan1.area() :
            //        yuan2.area()): yuan3.area();
        }
        System.out.println("圆面积最大"+maxGraphic1.area()+"\t圆周长最大"+maxGraphic1.perimeter());

        //矩形的比较
        Graphic maxGraphic2 = g2[0];
        for (int i = 1; i < g2.length; i++) {
            if (g2[i].area() > maxGraphic2.area()) {
                maxGraphic2 = g2[i];
            }
            if (g2[i].perimeter() > maxGraphic2.perimeter()) {
                maxGraphic2 = g2[i];
            }
        }
        System.out.println("矩形面积最大"+maxGraphic2.area()+"\t矩形周长最大"+maxGraphic2.perimeter());
    }
}