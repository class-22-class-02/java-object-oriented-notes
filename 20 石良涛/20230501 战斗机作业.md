# 1.作业

## 题目

1. 父类是：战斗机
   
       1. 属性：名称，颜色，载弹量，打击半径
       2. 功能：起飞，巡航，降落，雷达扫射，开火
   
   2. 接口：对空，对地
   
   3. 子类：
      
      1. 空空战斗机（只对空）
      2. 空地战斗机（能对空能对地）
      3. 轰炸机 （只能对地）
   
   4. 测试类：
      
      1. 生成每一种战斗并属性赋值，并调用相关功能



```java
// 战斗机类
public class Fighter {
    protected String name;
    protected String color;
    protected int payload;
    protected int attackRadius;

    public Fighter(String name, String color, int payload, int attackRadius) {
        this.name = name;
        this.color = color;
        this.payload = payload;
        this.attackRadius = attackRadius;
    }

    public void takeOff() {
        System.out.println(name + " takes off.");
    }

    public void cruise() {
        System.out.println(name + " cruises.");
    }

    public void land() {
        System.out.println(name + " lands.");
    }

    public void radarScan() {
        System.out.println(name + " radar scans.");
    }

    public void fire() {
        System.out.println(name + " fires.");
    }
}

// 对空接口
public interface AirToAir {
    void airToAirAttack();
}

// 对地接口
public interface AirToGround {
    void airToGroundAttack();
}

// 空空战斗机类
public class AirToAirFighter extends Fighter implements AirToAir {
    public AirToAirFighter(String name, String color, int payload, int attackRadius) {
        super(name, color, payload, attackRadius);
    }

    @Override
    public void airToAirAttack() {
        System.out.println(name + " attacks air targets.");
    }
}

// 空地战斗机类
public class AirToGroundFighter extends Fighter implements AirToAir, AirToGround {
    public AirToGroundFighter(String name, String color, int payload, int attackRadius) {
        super(name, color, payload, attackRadius);
    }

    @Override
    public void airToAirAttack() {
        System.out.println(name + " attacks air targets.");
    }

    @Override
    public void airToGroundAttack() {
        System.out.println(name + " attacks ground targets.");
    }
}

// 轰炸机类
public class Bomber extends Fighter implements AirToGround {
    public Bomber(String name, String color, int payload, int attackRadius) {
        super(name, color, payload, attackRadius);
    }

    @Override
    public void airToGroundAttack() {
        System.out.println(name + " attacks ground targets.");
    }
}

// 测试类
public class FighterTest {
    public static void main(String[] args) {
        // 创建每一种战斗机并属性赋值
        AirToAirFighter f1 = new AirToAirFighter("F-22 Raptor", "gray", 8, 20);
        AirToGroundFighter f2 = new AirToGroundFighter("F-35 Lightning II", "black", 6, 15);
        Bomber f3 = new Bomber("B-2 Spirit", "dark gray", 50, 50);

        // 调用相关功能
        f1.takeOff();
        f1.airToAirAttack();
        f1.radarScan();
        f1.land();

        f2.takeOff();
        f2.airToAirAttack();
        f2.airToGroundAttack();
        f2.fire();
        f2.land();

        f3.takeOff();
        f3.airToGroundAttack();
        f3.fire();
        f3.land();
    
}
}
   


```
