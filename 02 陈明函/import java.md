```java
//用键盘输入两个整数，并求和；如果输入的不是整数用异常去处理
import java.util.InputMismatchException;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        try {
            System.out.print("请输入第一个整数：");
            int num1 = sc.nextInt();

            System.out.print("请输入第二个整数：");
            int num2 = sc.nextInt();

            int sum = num1 + num2;
            System.out.println("两数之和为：" + sum);

        } catch (InputMismatchException e) {
            System.out.println("输入数字格式不正确，请重新输入。");
        }

        sc.close();
    }
}
```

