五一作业：

1. 父类是：战斗机

   1. 属性：名称，颜色，载弹量，打击半径
   2. 功能：起飞，巡航，降落，雷达扫射，开火

2. 接口：对空，对地

3. 子类：

   1. 空空战斗机（只对空）
   2. 空地战斗机（能对空能对地）
   3. 轰炸机 （只能对地）

4. 测试类：

   1. 生成每一种战斗并属性赋值，并调用相关功能

   ```java
   public abstract class Fighter {
       protected String name;
       protected String color;
       protected int payload;
       protected double attackRadius;
   
       public Fighter() {}
   
       public Fighter(String name, String color, int payload, double attackRadius) {
           this.name = name;
           this.color = color;
           this.payload = payload;
           this.attackRadius = attackRadius;
       }
   
       public void takeOff() {
           System.out.println(name + "起飞");
       }
   
       public void cruise() {
           System.out.println(name + "巡航");
       }
   
       public void landing() {
           System.out.println(name + "降落");
       }
   
       public abstract void radarSweep();
   
       public abstract void fire();
   }
   //接口AirToAir：
   
   java
   public interface AirToAir {
       public void airAttack();
   }
   //接口AirToGround：
   
   java
   public interface AirToGround {
       public void groundAttack();
   }
   //空空战斗机AAFighter，实现了AirToAir接口：
   
   java
   public class AAFighter extends Fighter implements AirToAir {
       public AAFighter() {}
   
       public AAFighter(String name, String color, int payload, double attackRadius) {
           super(name, color, payload, attackRadius);
       }
   
       @Override
       public void airAttack() {
           System.out.println(name + "对空攻击");
       }
   
       @Override
       public void radarSweep() {
           System.out.println(name + "雷达扫射");
       }
   
       @Override
       public void fire() {
           System.out.println(name + "开火");
       }
   }
   //地战斗机AGFighter，实现了AirToAir和AirToGround接口：
   
   java
   public class AGFighter extends Fighter implements AirToAir, AirToGround {
       public AGFighter() {}
   
       public AGFighter(String name, String color, int payload, double attackRadius) {
           super(name, color, payload, attackRadius);
       }
   
       @Override
       public void airAttack() {
           System.out.println(name + "对空攻击");
       }
   
       @Override
       public void groundAttack() {
           System.out.println(name + "对地攻击");
       }
   
       @Override
       public void radarSweep() {
           System.out.println(name + "雷达扫射");
       }
   
       @Override
       public void fire() {
           System.out.println(name + "开火");
       }
   }
   //轰炸机Bomber，实现了AirToGround接口：
   
   java
   public class Bomber extends Fighter implements AirToGround {
       public Bomber() {}
   
       public Bomber(String name, String color, int payload, double attackRadius) {
           super(name, color, payload, attackRadius);
       }
   
       @Override
       public void groundAttack() {
           System.out.println(name + "对地攻击");
       }
   
       @Override
       public void radarSweep() {
           System.out.println(name + "雷达扫射");
       }
   
       @Override
       public void fire() {
           System.out.println(name + "开火");
       }
   }
   //测试类：
   
   java
   public class Test {
       public static void main(String[] args) {
           AAFighter aaFighter = new AAFighter("AA-Eagle", "银色", 6, 120.0);
           aaFighter.takeOff();
           aaFighter.cruise();
           aaFighter.radarSweep();
           aaFighter.airAttack();
           aaFighter.fire();
           aaFighter.landing();
   
           AGFighter agFighter = new AGFighter("AG-Raptor", "黑色", 4, 90.0);
           agFighter.takeOff();
           agFighter.cruise();
           agFighter.radarSweep();
           agFighter.airAttack();
           agFighter.groundAttack();
           agFighter.fire();
           agFighter.landing();
   
           Bomber bomber = new Bomber("B-2", "灰色", 20, 500.0);
           bomber.takeOff();
           bomber.cruise();
           bomber.radarSweep();
           bomber.groundAttack();
           bomber.fire();
           bomber.landing();
       }
   }
   ```

   