### 图形

（1）父类Graphic图形

- public double area()方法：返回0.0
- public double perimeter()方法：返回0.0
- public String getInfo()方法，返回图形面积和图形周长

（2）子类Circle圆继承Graphic图形

- 包含属性：radius，属性私有化
- 包含get/set方法
- 重写area()求面积方法
- 重写perimeter()求周长方法
- 重写getInfo()方法，返回圆的半径，面积和周长

（3）子类矩形Rectangle继承Graphic图形

- 包含属性：length、width，属性私有化
- 包含get/set方法
- 重写area()求面积方法
- 重写perimeter()求周长方法
- 重写getInfo()方法，返回长和宽，面积、周长信息

（4）在测试类中，新建一个比较图形面积的方法，再建一个比较图形周长的方法，main方法中创建多个圆和矩形对象,再调用方法比较他们的周长或面积。

```java
package aaa;

import java.util.Scanner;

public class Graphic {
    public double area(){
        return 0.0;
    }
    public double perimeter(){
        return 0.0;
    }
    public String getInfo(){
        return "图形面积为"+area()+"图形周长为"+perimeter();
    }
}
class Circle extends Graphic{
    private int radius;

    public Circle(){}
    public Circle(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }
    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public double area() {
        return 3.14*radius*radius;
    }

    @Override
    public double perimeter() {
        return 2*3.14*radius;
    }

    @Override
    public String getInfo() {
        return "圆的半径为"+radius+super.getInfo();
    }
}
class Rectangle extends Graphic{
    private int length;
    private int width;

    public Rectangle(){}
    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }
    public int getWidth() {
        return width;
    }
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public double area() {
        return length*width;
    }

    @Override
    public double perimeter() {
        return 2*length+2*width;
    }

    @Override
    public String getInfo() {
        return "长为"+length+"宽为"+width+super.getInfo();
    }
}
class test{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Circle A= new Circle();
        System.out.println("请输入第一个圆的半径");
        A.setRadius(scanner.nextInt());
        Circle B= new Circle();
        System.out.println("请输入第二个圆的半径");
        B.setRadius(scanner.nextInt());
        Rectangle C = new Rectangle();
        System.out.println("请输入矩形1的长");
        C.setLength(scanner.nextInt());
        Rectangle D = new Rectangle();
        System.out.println("请输入矩形2的长");
        D.setLength(scanner.nextInt());
        System.out.println("请输入矩形1的宽");
        C.setWidth(scanner.nextInt());
        System.out.println("请输入矩形2的宽");
        D.setWidth(scanner.nextInt());
        System.out.println("圆1:\n" +A.getInfo());
        System.out.println("圆2:\n" +B.getInfo());
        System.out.println();
        System.out.println("矩形1:\n" +C.getInfo());
        System.out.println("矩形2:\n" + D.getInfo());
        System.out.println();

    }
}
```