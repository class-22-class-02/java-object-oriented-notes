

**父类是：战斗机**

1. **属性：名称，颜色，载弹量，打击半径**
2. **功能：起飞，巡航，降落，雷达扫射，开火**
3. **接口：对空，对地**
4. 子类：
   1. **空空战斗机（只对空）**
   2. **空地战斗机（能对空能对地）**
   3. **轰炸机 （只能对地）**
5. 测试类：
   1. **生成每一种战斗并属性赋值，并调用相关功能**

父类：

```java
public class Warcraft {
    private String name;
    private String colour;
    private double bombLoad;
    private double strikeRadius;

    public Warcraft() {
    }

    public Warcraft(String name, String colour, double bombLoad, double strikeRadius) {
        this.name = name;
        this.colour = colour;
        this.bombLoad = bombLoad;
        this.strikeRadius = strikeRadius;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public double getBombLoad() {
        return bombLoad;
    }

    public void setBombLoad(double bombLoad) {
        this.bombLoad = bombLoad;
    }

    public double getStrikeRadius() {
        return strikeRadius;
    }

    public void setStrikeRadius(double strikeRadius) {
        this.strikeRadius = strikeRadius;
    }

    public void fly(){
        System.out.println(name + "起飞");
    }

    public void  cruise(){
        System.out.println(name+"正在巡航");
    }

    public void landing(){
        System.out.println(name+"正在降落");
    }

    public void radarScanning(){
        System.out.println(name + "正在执行雷达扫描");
    }

    public void fire(){
        System.out.println(name+"正在开火");
    }
}
接口1 对空：

public interface AirAttack {
    void AirAttack();
}
接口2 对地：

public interface Bomb {
    void Bomb();
}
接口3 对空对地：

public interface GroundAttack {
    void GroundAttack();
}
子类1 对空战斗机：

public class AirToAirFighter extends Warcraft implements AirAttack {


    public AirToAirFighter() {
    }

    public AirToAirFighter(String name, String colour, double bombLoad, double strikeRadius) {
        super(name, colour, bombLoad, strikeRadius);
    }

    public AirToAirFighter(String s) {
    }

    @Override
    public void AirAttack() {
        System.out.println(getName() + "正在对空扫射");
    }
}
子类2 轰炸机:

public class Bomber extends Warcraft implements Bomb{
    public Bomber() {
    }

    public Bomber(String name, String colour, double bombLoad, double strikeRadius) {
        super(name, colour, bombLoad, strikeRadius);
    }

    @Override
    public void Bomb() {
        System.out.println(getName()+"正在对地面轰炸");
    }
}
子类3 对空对地战斗机：

public class AirToGroundFighter extends Warcraft implements GroundAttack{
    public AirToGroundFighter() {
    }

    public AirToGroundFighter(String name, String colour, double bombLoad, double strikeRadius) {
        super(name, colour, bombLoad, strikeRadius);
    }

    @Override
    public void GroundAttack() {
        System.out.println(getName() + "正在上下扫射");
    }
}
测试类

public class Test {
    public static void main(String[] args) {
        //生成对空战斗机
        AirToAirFighter air = new AirToAirFighter("B-1B","黑色",1000,100);
        air.fly();
        air.cruise();
        air.radarScanning();
        air.AirAttack();
        air.landing();
        //生成轰炸机
        Bomber bomber = new Bomber("B-17","白色",10000,50);
        bomber.fly();
        bomber.cruise();
        bomber.radarScanning();
        bomber.Bomb();
        bomber.landing();
        //生成空地战斗机
        AirToGroundFighter groundFighter = new AirToGroundFighter("B-21","灰色",2000,50);
        groundFighter.fly();
        groundFighter.cruise();
        groundFighter.radarScanning();
        groundFighter.GroundAttack();
        groundFighter.landing();
    }
}
```