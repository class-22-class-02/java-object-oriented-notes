```java
public class Student {
    private String name;
    private String id;
    private char sex;
    private int age;

    public Student(String name, String id, char sex, int age) {
        this.name = name;
        this.id = id;
        this.sex = sex;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        if(name.length()==3) return id+"\t\t"+name+"\t\t"+sex+"\t\t"+age;
        else return id+"\t\t"+name+"\t\t\t"+sex+"\t\t"+age;
    }
}
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Stu {
    public static Scanner sc = new Scanner(System.in);//扫描器

    public static void main(String[] args) {
        ArrayList<Student> stu = new ArrayList<>(); //储存学生类
        while (true) {
            System.out.println("-- 欢迎使用学生管理系统 --\n" +
                    "\t1 添加学生信息\n" +
                    "\t2 删除学生信息\n" +
                    "\t3 修改学生信息\n" +
                    "\t4 查询学生信息\n" +
                    "\t5 查询所有学生信息\n" +
                    "\t6 退出系统\n" +
                    "-- 输入对应数字使用相关功能 --");
            String n = sc.next();
            switch (n) {
                case "1":
                    stuadd(stu);
                    break;
                case "2":
                    sture(stu);
                    break;
                case "3":
                    stuset(stu);
                    break;
                case "4":
                    stuget(stu);
                    break;
                case "5":
                    stugetall(stu);
                    break;
                case "6":
                    System.out.println("感谢使用");
                    System.exit(0);
                    break;
                default:
                    System.out.println("别乱jb输入");
            }
        }
    }

    private static void stugetall(ArrayList<Student> list) {
        if (list.size() == 0) {
            cls();
            System.out.println("暂无学生数据");
        } else {
            cls();
            System.out.println("学号\t\t姓名\t\t\t性别\t\t年龄");
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i));
            }
        }
    }

    private static void stuset(ArrayList<Student> list) {
        if (list.size() == 0) {
            cls();
            System.out.println("暂无学生数据");
        } else {
            System.out.println("请输入要修改的学生学号或姓名");
            String n = sc.next();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getName().equals(n) || list.get(i).getId().equals(n)) {
                    System.out.println("请输入修改后学号");
                    String id = sc.next();
                    System.out.println("请输入修改后姓名");
                    String name = sc.next();
                    System.out.println("请输入修改后性别");
                    char sex = sc.next().charAt(0);
                    System.out.println("请输入修改后年龄");
                    int age = sc.nextInt();
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(i).getId().equals(id)) {
                            cls();
                            list.set(i, new Student(name, id, sex, age));
                            System.out.println("修改成功");
                            return;
                        } else if (list.get(j).getId().equals(id)) {
                            cls();
                            System.out.println("学号重复请重新修改");
                            return;
                        }
                    }
                    cls();
                    list.set(i, new Student(name, id, sex, age));
                    System.out.println("修改成功");
                }
            }
        }
        cls();
        System.out.println("无该学生");
    }//修改方法

    private static void stuget(ArrayList<Student> list) {
        if (list.size() == 0) {
            cls();
            System.out.println("暂无学生数据");
        } else {
            System.out.println("请输入要查询的学生学号或姓名");
            String n = sc.next();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getName().equals(n) || list.get(i).getId().equals(n)) {
                    System.out.println("学号\t\t姓名\t\t\t性别\t\t年龄");
                    cls();
                    System.out.println(list.get(i));
                    return;
                }
            }
            cls();
            System.out.println("无该学生");
        }
    }//查询方法

    private static void sture(ArrayList<Student> list) {
        if (list.size() == 0) {
            cls();
            System.out.println("暂无学生数据");
        } else {
            System.out.println("请输入要删除的学生学号或姓名");
            String n = sc.next();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getName().equals(n) || list.get(i).getId().equals(n)) {
                    cls();
                    list.remove(i);
                    System.out.println("删除成功");
                    return;
                }
            }
            cls();
            System.out.println("无该学生");
        }
    }//删除方法

    public static void stuadd(ArrayList<Student> list) {
        System.out.println("请输入学号");
        String id = sc.next();
        System.out.println("请输入姓名");
        String name = sc.next();
        System.out.println("请输入性别");
        char sex = sc.next().charAt(0);
        System.out.println("请输入年龄");
        int age = sc.nextInt();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId().equals(id)) {
                cls();
                System.out.println("学号重复请重新添加");
                return;
            } else {
                cls();
                list.add(new Student(name, id, sex, age));
                System.out.println("添加成功");
                return;
            }
        }
        cls();
        list.add(new Student(name, id, sex, age));
        System.out.println("添加成功");
    }//添加方法

    public static void cls() {
        for (int i = 0; i < 100; i++) {
            System.out.println();
        }
    }//伪cls
}
```