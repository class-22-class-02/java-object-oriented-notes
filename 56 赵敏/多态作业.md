```java
package d0508;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        //扫描器
        Scanner scanner = new Scanner(System.in);

        //圆1
        Circle circle = new Circle();
        System.out.println("请输入圆1的半径");
        circle.setRadius(scanner.nextDouble());
        //圆2
        Circle circle2 = new Circle();
        System.out.println("请输入圆2的半径");
        circle2.setRadius(scanner.nextDouble());
        //矩形1
        Rectangle rectangle = new Rectangle();
        System.out.println("请输入矩形1的长");
        rectangle.setLength(scanner.nextDouble());
        System.out.println("请输入矩形1的宽");
        rectangle.setWidth(scanner.nextDouble());
        //矩形2
        Rectangle rectangle2 = new Rectangle();
        System.out.println("请输入矩形2的长");
        rectangle2.setLength(scanner.nextDouble());
        System.out.println("请输入矩形2的宽");
        rectangle2.setWidth(scanner.nextDouble());

        System.out.println("圆1:\n" + circle.getInfo());
        System.out.println("圆2:\n" + circle2.getInfo());
        System.out.println();
        System.out.println("矩形1:\n" + rectangle.getInfo());
        System.out.println("矩形2:\n" + rectangle2.getInfo());
        System.out.println();
        judgeAreaRectangle(rectangle.area(), rectangle2.area());
        judgeAreaCircle(circle.area(), circle2.area());
        judgePerimeterRectangle(rectangle.perimeter(), rectangle2.perimeter());
        judgePerimeterCircle(rectangle.perimeter(), rectangle2.perimeter());

    }

    public static String judgeAreaRectangle(double e1, double e2) {
        System.out.println("两个矩形的面积比较");
        String result = e1 > e2 ? "\t矩形1大于矩形2\n" : "\t矩形2大于矩形1\n";
        System.out.println(result);
        return null;
    }

    public static String judgeAreaCircle(double e1, double e2) {
        System.out.println("两个圆的面积比较");
        String result = e1 > e2 ? "\t圆1大于圆2\n" : "\t圆2大于圆1\n";
        System.out.println(result);
        return null;
    }


    public static String judgePerimeterCircle(double e1, double e2) {
        System.out.println("两个圆的周长比较");
        String result = e1 > e2 ? "\t圆1大于圆2\n" : "\t圆2大于圆1\n";
        System.out.println(result);
        return null;
    }

    public static String judgePerimeterRectangle(double e1, double e2) {
        System.out.println("两个矩形的周长比较");
        String result = e1 > e2 ? "\t矩形1大于矩形2\n" : "\t矩形2大于矩形1\n";
        System.out.println(result);
        return null;
    }

}
```

```java
package d0508;

public class Graphic {
    public double area() {
        return 0.0;
    }

    public double perimeter() {
        return 0.0;
    }

    public String getInfo() {
        return "图形面积和图形周长";
    }
}
```

```java
package d0508;

import java.text.DecimalFormat;

public class Circle extends Graphic {
    private double radius;

    DecimalFormat df = new DecimalFormat("#.##");

    @Override
    public String getInfo() {
        return "\t半径：" + radius +
                "\t周长：" + df.format(perimeter()) +
                "\t面积：" + df.format(area());

    }

    @Override
    public double area() {
        double area = Math.PI * this.radius * this.radius;
        return area;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override// 周长
    public double perimeter() {
        double perimeter = 2 * radius * Math.PI;
        return perimeter;
    }
 
```

```java
package d0508;

import java.text.DecimalFormat;


public class Rectangle extends Graphic {
    private double length, width;

    @Override
    public double area() {
        double area = width * length;
        return area;
    }

    @Override
    public double perimeter() {
        double perimeter = (length + width) * 2;
        return perimeter;
    }

    DecimalFormat df = new DecimalFormat("2");

    @Override
    public String getInfo() {
        return "\t长:" + df.format(length) +
                "\t宽:" + df.format(width) +
                "\t周长：" + df.format(perimeter()) +
                "\t面积" + df.format(area());
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
```

