# 作业

### 第一题案例

~~~ java
// MAP4
package demo1;

public class MAP4 implements Player{
        @Override
        public void play() {
            System.out.println("可以播放音乐");
        }

        @Override
        public void pause() {
            System.out.println("可以暂停音乐");
        }

        @Override
        public void stop() {
            System.out.println("可以停止播放音乐");
        }
    }

// MP3
package demo1;

public  class MP3 implements Player{

        @Override
        public void play() {
            System.out.println("可以播放音乐");
        }

        @Override
        public void pause() {
            System.out.println("可以暂停音乐");
        }

        @Override
        public void stop() {
            System.out.println("可以停止播放音乐");
        }
    }


// 手机
package demo1;

public class MobilePhone implements Player{
        @Override
        public void play() {
            System.out.println("可以播放音乐");
        }

        @Override
        public void pause() {
            System.out.println("可以暂停音乐");
        }

        @Override
        public void stop() {
            System.out.println("可以停止播放音乐");
        }
    }

// 接口
package demo1;

public interface Player{
    void play();
    void pause();
    void stop();
}

// 测试类
package demo1;

public class Test {
    public static void main(String[] args) {
        MP3 mp3 = new MP3();
        System.out.println("MP3");
        mp3.play();
        mp3.pause();
        mp3.stop();
        System.out.println("————————————————————————");
        MAP4 map4 = new MAP4();
        System.out.println("MAP4");
        map4.play();
        map4.pause();
        map4.stop();
        System.out.println("————————————————————————");
        MobilePhone mobilePhone = new MobilePhone();
        System.out.println("手机");
        mobilePhone.play();
        mobilePhone.pause();
        mobilePhone.stop();
    }
}

~~~

### 第二题 飞机

##### 题目

```ABAP
//1. 父类是：战斗机
//        1. 属性：名称，颜色，载弹量，打击半径
//        2. 功能：起飞，巡航，降落，雷达扫射，开火

//        3. 子类：
//        1. 空空战斗机（只对空）
//        2. 空地战斗机（能对空能对地）
//        3. 轰炸机 （只能对地）
//
//        4. 测试类：
//        1. 生成每一种战斗并属性赋值，并调用相关功能
```

~~~ java
// 父类 战斗机
package demo2;

public abstract class Warplane {
    String name; // 名称
    String color; // 颜色
    String BombLoad; // 载弹量
    String StrikeRadius; // 打击半径

    public abstract void TakeOff();  // 起飞
    public abstract void Cruise();   // 巡航
    public abstract void descent();  // 降落
    public abstract void RadarStrafing();   // 雷达扫射

    public Warplane(String name, String color, String bombLoad, String strikeRadius) {
        this.name = name;
        this.color = color;
        BombLoad = bombLoad;
        StrikeRadius = strikeRadius;
    }

    public Warplane() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBombLoad() {
        return BombLoad;
    }

    public void setBombLoad(String bombLoad) {
        BombLoad = bombLoad;
    }

    public String getStrikeRadius() {
        return StrikeRadius;
    }

    public void setStrikeRadius(String strikeRadius) {
        StrikeRadius = strikeRadius;
    }

    public abstract void show();
}


// 子类 空空战斗机(只对空)
package demo2;

public class SkyWarplane extends Warplane implements Sky{

    public SkyWarplane(String name, String color, String bombLoad, String strikeRadius) {
        super(name, color, bombLoad, strikeRadius);
    }

    public SkyWarplane() {
    }

    @Override
    public void show() {
        System.out.println("对空战斗机\n--------\n"+"飞机名字:"
                +getName()+"\t\t机身颜色:"
                +getColor()+"\t\t\t飞机载弹量:"
                +getBombLoad()+"\t\t\t飞机打击半径:"
                +getStrikeRadius());
    }

    @Override
    public void TakeOff() {
        System.out.print("空域已清空,可以起飞");
    }

    @Override
    public void Cruise() {
        System.out.print("\t\t接到任务,进行巡航");
    }

    @Override
    public void descent() {
        System.out.print("\t\t跑道已清空，可以进行降落");
    }

    @Override
    public void RadarStrafing() {
        System.out.println("\t\t发现敌机，可以进行雷达扫射");
    }
}


// 子类 空地战斗机（能对空，能对地）
package demo2;

public class LandWarplane extends Warplane implements Land{

    public LandWarplane(String name, String color, String bombLoad, String strikeRadius) {
        super(name, color, bombLoad, strikeRadius);
    }

    public LandWarplane() {
    }

    @Override
    public void show() {
        System.out.println("可对空,可对地战斗机\n--------\n"+"飞机名字:"
                +getName()+"\t\t机身颜色:"
                +getColor()+"\t\t\t飞机载弹量:"
                +getBombLoad()+"\t\t\t飞机打击半径:"
                +getStrikeRadius());
    }

    @Override
    public void TakeOff() {
        System.out.print("空域已清空,可以起飞");
    }

    @Override
    public void Cruise() {
        System.out.print("\t\t接到任务,进行巡航");
    }

    @Override
    public void descent() {
        System.out.print("\t\t跑道已清空，可以进行降落");
    }

    @Override
    public void RadarStrafing() {
        System.out.println("\t\t发现敌机，可以进行雷达扫射");
    }
}

// 轰炸机（只对地）
package demo2;

public class Bomber extends Warplane implements Land {

    public Bomber(String name, String color, String bombLoad, String strikeRadius) {
        super(name, color, bombLoad, strikeRadius);
    }

    public Bomber() {
    }

    @Override
    public void show() {
        System.out.println("对地轰炸机\n--------\n"+"飞机名字:"
                +getName()+"\t\t机身颜色:"
                +getColor()+"\t\t飞机载弹量:"
                +getBombLoad()+"\t\t\t飞机打击半径:"
                +getStrikeRadius());
    }

    @Override
    public void TakeOff() {
        System.out.print("空域已清空,可以起飞");
    }

    @Override
    public void Cruise() {
        System.out.print("\t\t接到任务,进行巡航");
    }

    @Override
    public void descent() {
        System.out.print("\t\t跑道已清空，可以进行降落");
    }

    @Override
    public void RadarStrafing() {
        System.out.println("\t\t发现敌机，可以进行雷达扫射");
    }
}

// 测试类
package demo2;

public class Test {
    public static void main(String[] args) {
        SkyWarplane sky = new SkyWarplane("歼-10","白色","7吨","1200公里");
        sky.show();
        sky.TakeOff();
        sky.Cruise();
        sky.descent();
        sky.RadarStrafing();
        System.out.println("-------------------------------------------------------------------------------------");
        LandWarplane land = new LandWarplane("歼-20","灰色","6枚导弹","2000公里");
        land.show();
        land.TakeOff();
        land.Cruise();
        land.descent();
        land.RadarStrafing();
        System.out.println("-------------------------------------------------------------------------------------");
        Bomber bomber = new Bomber("轰-18","天空蓝","15吨","2000公里");
        bomber.show();
        bomber.TakeOff();
        bomber.Cruise();
        bomber.descent();
        bomber.RadarStrafing();


    }
}

~~~

