```java
public class Graphic {
    public double area() {
        return 0.0;
    }

    public double perimeter() {
        return 0.0;
    }

    public String getInfo() {
        String Graphic=null;
        return Graphic;
    }

}
class Circle extends Graphic{
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    @Override
    public double area() {
        return radius*radius*Math.PI;
    }

    @Override
    public double perimeter() {
        return radius*2*Math.PI;
    }

    @Override
    public String getInfo() {
        return "半径："+radius+"面积："+radius*radius*Math.PI+"周长："+radius*2*Math.PI;
    }
}

class Rectangle extends Graphic{
    private double length;
    private double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public Rectangle() {
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double area() {
        return length*width;
    }

    @Override
    public double perimeter() {
        return (length+width)*2;
    }

    @Override
    public String getInfo() {
        return "长："+length+"宽："+width+"面积："+length*width+"周长："+(length+width)*2;
    }
}
public class Test {
    public static void main(String[] args) {
        Graphic graphic = new Circle(3);
        double yuanmj=graphic.area();
        double yuanzc= graphic.perimeter();
        String a = graphic.getInfo();
        System.out.println("圆"+a);
        Graphic rectangle = new Rectangle(3,3);
        double jumj= rectangle.area();
        double juzc= rectangle.perimeter();
        String b = rectangle.getInfo();
        System.out.println("矩形"+b);
        boolean mj=Bjmj(yuanmj,jumj);
        System.out.println("圆形面积大于矩形面积是："+mj);
        boolean zc=Bjzc(yuanzc,juzc);
        System.out.println("圆形周长大于矩形周长是："+zc);
    }

    private static boolean Bjzc(double yuanzc, double juzc) {
        return yuanzc>juzc;
    }
    private static boolean Bjmj(double yuanmj,double jumj) {
        return yuanmj>jumj;
    }
}

```