```java
package Day0503;

public class Warplane {
        //1. 父类是：战斗机
        //1. 属性：名称，颜色，载弹量，打击半径
        //2. 功能：起飞，巡航，降落，雷达扫射，开火
        String name;
        String color;
        String bombLoad;
        int StrikingRange;
        void take_off(){
            System.out.print("起飞、");
        }
        void cruise(){
            System.out.print("巡航、");
        }
        void descent(){
            System.out.print("降落、");
        }
        void Radar_strafing(){
            System.out.print("雷达扫射、");
        }
        void fire(){
            System.out.println("开火 ");
        }
    }
    interface Connector1{
        //接口：对空
        void Antiaircraft();
    }
    interface Connector2{
        //接口：对地
        void over_the_ground();
    }
    class KkWarplane extends Warplane implements Connector1{
        //1. 空空战斗机（只对空）
        @Override
        public void Antiaircraft() {
            System.out.print("对空的功能是：");
        }
    }
    class KdWarplane extends Warplane implements Connector1{
        //2. 空地战斗机（能对空能对地）
        @Override
        public void Antiaircraft() {
            System.out.print("对空和对地的功能是");
        }

        }

    class Bomber extends Warplane implements Connector2{
        //3. 轰炸机 （只能对地）
        @Override
        public void over_the_ground() {
            System.out.print("对地的功能是：");
        }
    }




测试类：
 package Day0503;

public class Test {
        //3. 测试类：
        //1. 生成每一种战斗并属性赋值，并调用相关功能
        public static void main(String[] args) {
            KkWarplane kk = new KkWarplane();
            kk.name="歼教1";
            kk.color="红色";
            kk.bombLoad="六发";
            kk.StrikingRange=4815;
            System.out.println(kk.color+kk.name+"："+"战斗机载弹量"+kk.bombLoad+","+"射程："+kk.StrikingRange+";");
            kk.Antiaircraft();
            kk.take_off();
            kk.cruise();
            kk.descent();
            kk.fire();

            KdWarplane kd = new KdWarplane();
            kd.name="米格-35";
            kd.color="灰色";
            kd.bombLoad="十发";
            kd.StrikingRange=3100;
            System.out.println(kd.color+kk.name+"："+"战斗机载弹量"+kd.bombLoad+","+"射程："+kd.StrikingRange+";");
            kd.Antiaircraft();
            kd.take_off();
            kd.cruise();
            kd.descent();
            kd.Radar_strafing();
            kd.fire();

            Bomber bo = new Bomber();
            bo.name="歼-10";
            bo.color="深灰色";
            bo.bombLoad="五发";
            bo.StrikingRange=1850;
            System.out.println(bo.color+bo.name+"："+"战斗机载弹量"+bo.bombLoad+","+"射程："+bo.StrikingRange+";");            bo.over_the_ground();
            kd.take_off();
            kd.descent();
            kd.Radar_strafing();

        }
    }

```

