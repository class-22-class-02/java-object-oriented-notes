## 多态练习

### 1、图形

（1）父类Graphic图形

- public double area()方法：返回0.0
- public double perimeter()方法：返回0.0
- public String getInfo()方法，返回图形面积和图形周长

（2）子类Circle圆继承Graphic图形

- 包含属性：radius，属性私有化
- 包含get/set方法
- 重写area()求面积方法
- 重写perimeter()求周长方法
- 重写getInfo()方法，返回圆的半径，面积和周长

（3）子类矩形Rectangle继承Graphic图形

- 包含属性：length、width，属性私有化
- 包含get/set方法
- 重写area()求面积方法
- 重写perimeter()求周长方法
- 重写getInfo()方法，返回长和宽，面积、周长信息

（4）在测试类中，新建一个比较图形面积的方法，再建一个比较图形周长的方法，main方法中创建多个圆和矩形对象,再调用方法比较他们的周长或面积。

```java
public class Graphic {
// 1、图形

//（1）父类Graphic图形

// public double area()方法：返回0.0
// public double perimeter()方法：返回0.0
// public String getInfo()方法，返回图形面积和图形周长

//（2）子类Circle圆继承Graphic图形

// 包含属性：radius，属性私有化
// 包含get/set方法
// 重写area()求面积方法
// 重写perimeter()求周长方法
// 重写getInfo()方法，返回圆的半径，面积和周长

//（3）子类矩形Rectangle继承Graphic图形

// 包含属性：length、width，属性私有化
// 包含get/set方法
// 重写area()求面积方法
// 重写perimeter()求周长方法
// 重写getInfo()方法，返回长和宽，面积、周长信息

//（4）在测试类中，新建一个比较图形面积的方法，再建一个比较图形周长的方法，main方法中创建多个圆和矩形对象,再调用方法比较他们的周长或面积
    public double area(){
        return 0.0;
    }

    public double perimeter(){
        return 0.0;
    }

    public String getInfo(){
        return "面积："+area()+"周长："+perimeter();
    }

}

class Circle extends Graphic{

      private int radius;

        public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
    @Override
    public double area() {
        return super.area();
    }

    @Override
    public double perimeter() {
        return super.perimeter();
    }

    @Override
    public String getInfo() {
        return super.getInfo();
    }
}

class Rectangle extends Graphic{
        private double length;
        private double width;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double area() {
        return super.area();
    }

    @Override
    public double perimeter() {
        return super.perimeter();
    }

    @Override
    public String getInfo() {
        return super.getInfo();
    }
```

```java
public class Test {
    public static void main(String[] args) {

        //圆
        Circle c = new Circle();
        c.setRadius(1545);
        Circle b = new Circle();
        b.setRadius(1545);
        //比较面积
        System.out.println(areas(c,b));

        //矩形
        Rectangle r = new Rectangle();
        r.setLength(5465.12);
        r.setWidth(5453546);
        Rectangle r1 = new Rectangle();
        r1.setLength(5465.12);
        r1.setWidth(5453546);
        //比较周长
        System.out.println(perimeters(r1,r));
    }

    //比较面积
    public static boolean areas(Graphic a ,Graphic b){
        return a.area()>b.area();
    }
    //比较周长
    public static boolean perimeters(Graphic a ,Graphic b){
        return a.perimeter()>b.perimeter();
    }
```

