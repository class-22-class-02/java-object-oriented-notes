```java
import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("请输入两个整数");
            String num1 = sc.next();
            String num2 = sc.next();
            try {
               int n1 = Integer.parseInt(num1);
               int n2 = Integer.parseInt(num2);
               int num = n1 + n2;
               System.out.println("两个整数的和是" + num);
               break;
            } catch (NumberFormatException e) {
                System.out.println("只能输入整数,请重新输入");
            }
        }
    }
}
```